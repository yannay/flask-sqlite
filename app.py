from flasgger import Swagger

from controller.kettle import Kettle
from dao.db import create_tables
from dao.logs import get_logs  # , get_by_id  ,delete_log, insert_log, update_log
from flask import Flask, Response, jsonify, request

app_name = "flask-sqlite"
app = Flask(app_name)
swagger = Swagger(app)
app.config["JSON_SORT_KEYS"] = False
kettle = Kettle()


# get logs from database
@app.route("/logs", methods=["GET"])
def get_logs_all() -> Response:
    """
    Endpoint returning a list of logs
    This is using docstrings for specifications.
    ---
    definitions:
      Logs:
        type: list
    responses:
      200:
        description: A list of logs
        schema:
          $ref: '#/definitions/Logs'
        examples:
          log: [ "5", "current temperature = 50", "2023-02-14 15:03:12.297751"]
    """
    games = get_logs()
    return jsonify(games)


# execute new command
@app.route("/command", methods=["POST"])
def add_command() -> Response:
    """
    Endpoint creating a new command
    List of commands:
    turn on
    turn off
    pour
    ---
    consumes:
      - application/json
    parameters:
      - name: body
        in: body
        required: true
        schema:
          id : toto
          required:
            - message
          properties:
            message:
              type: string
              description: Command
    definitions:
      Status:
        type: string
    responses:
      200:
        description: Kettle status
        schema:
          $ref: '#/definitions/Status'
        examples:
          status: [ "turned off"]
    """
    game_details = request.get_json()
    message = game_details["message"]
    result = kettle.parse_message(message)
    # result = insert_log(message)
    return jsonify(result)


# @app.route("/log", methods=["PUT"])
# def update_log():
#     game_details = request.get_json()
#     id = game_details["id"]
#     message = game_details["message"]
#     result = update_log(id, message)
#     return jsonify(result)


# @app.route("/log/<id>", methods=["DELETE"])
# def delete_log(id):
#     result = delete_log(id)
#     return jsonify(result)


# @app.route("/log/<id>", methods=["GET"])
# def get_log_by_id(id):
#     game = get_by_id(id)
#     return jsonify(game)

if __name__ == "__main__":
    create_tables()
    app.run(host="0.0.0.0", port=8001, debug=False)
