from time import sleep

from config import TIME_TO_BOIL, WATER_AMOUNT

# from datetime import datetime
from dao.logs import insert_log


class Kettle:
    def __init__(self) -> None:
        self.status = "turned off"
        self.vol = 0.0
        # self.time = datetime.now()

    # turn off kettle
    def __turn_on(self) -> None:
        self.status = "turned on"
        insert_log("turned on")
        for i in range(TIME_TO_BOIL):
            if self.status == "turned on":
                sleep(1)
                insert_log(f"current temperature = {(100/TIME_TO_BOIL) * (i+1)}")
            elif self.status == "turned off":
                return
        insert_log("boiled up")
        self.__turn_off()

    # turn on kettle
    def __turn_off(self) -> None:
        self.status = "turned off"
        insert_log("turned off")

    # check input message
    def parse_message(self, message: str) -> str:
        if message == "turn on":
            self.__turn_on()
        elif message == "turn off":
            insert_log("stopped")
            self.__turn_off()
        elif "pour" in message:
            message_list = message.split()
            try:
                vol = float(message_list[1])
                if vol > 0 and vol <= WATER_AMOUNT:
                    if vol + self.vol <= WATER_AMOUNT:
                        self.vol = self.vol + vol
                        insert_log(message)
                    else:
                        self.vol = WATER_AMOUNT
                        insert_log("full kettle")
                else:
                    insert_log(f"bad argument for pour: {message}")
            except Exception:
                insert_log(f"bad argument for pour: {message}")
        else:
            insert_log(f"bad command: {message}")
        return self.status
