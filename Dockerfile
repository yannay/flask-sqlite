FROM python:3.9

WORKDIR /usr/src/app

# установка нужного часового пояса
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN date

# install by pip
# COPY requirements.txt ./
# RUN pip install --no-cache-dir -r requirements.txt

# install by poetry
RUN curl -sSL https://install.python-poetry.org | POETRY_HOME=/etc/poetry POETRY_CACHE_DIR=/etc/poetry/.cache/pypoetry python3 -
ENV PATH="$PATH:/etc/poetry/bin"
RUN poetry --version
COPY pyproject.toml poetry.lock ./
# RUN poetry config cache-dir /etc/poetry/.cache/pypoetry
RUN poetry config virtualenvs.create false --local && poetry install --no-dev
RUN chmod +r poetry.toml

COPY . ./

EXPOSE 8001
CMD ["poetry", "run", "python3", "app.py"]
#CMD ["uvicorn", "app:app", "--host", "0.0.0.0", "--port", "8000"]