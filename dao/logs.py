from datetime import datetime
from typing import Any

from dao.db import get_db


def insert_log(message: str) -> bool:
    db = get_db()
    cursor = db.cursor()
    time = datetime.now()
    statement = "INSERT INTO logs(message, time) VALUES (?, ?)"
    cursor.execute(statement, [message, time])
    db.commit()
    return True


def update_log(id: str, message: str) -> bool:
    db = get_db()
    cursor = db.cursor()
    statement = "UPDATE logs SET message = ? WHERE id = ?"
    cursor.execute(statement, [message, id])
    db.commit()
    return True


def delete_log(id: str) -> bool:
    db = get_db()
    cursor = db.cursor()
    statement = "DELETE FROM logs WHERE id = ?"
    cursor.execute(statement, [id])
    db.commit()
    return True


def get_by_id(id: str) -> Any:
    db = get_db()
    cursor = db.cursor()
    statement = "SELECT id, message, time FROM logs WHERE id = ?"
    cursor.execute(statement, [id])
    return cursor.fetchone()


def get_logs() -> Any:
    db = get_db()
    cursor = db.cursor()
    query = "SELECT id, message, time FROM logs"
    cursor.execute(query)
    return cursor.fetchall()
