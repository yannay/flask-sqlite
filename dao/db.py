import sqlite3

DATABASE_NAME = "sqlite.db"


def get_db() -> sqlite3.Connection:
    conn = sqlite3.connect(DATABASE_NAME)
    return conn


def create_tables() -> None:
    tables = [
        """CREATE TABLE IF NOT EXISTS logs(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                message TEXT NOT NULL,
                time TEXT NOT NULL
            )
            """
    ]
    db = get_db()
    cursor = db.cursor()
    for table in tables:
        cursor.execute(table)
